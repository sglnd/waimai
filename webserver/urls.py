#!coding=utf-8
from django.conf.urls import patterns,include
from tastypie.api import Api
from webserver.handlers import BusinessAreaResource, CatagoryResource,\
    GoodsResource, OrderItemResource, OrdersResource

v1_api = Api(api_name='v1')
v1_api.register(BusinessAreaResource())
v1_api.register(CatagoryResource())
v1_api.register(GoodsResource())
v1_api.register(OrdersResource())
v1_api.register(OrderItemResource())


urlpatterns = patterns('',
    (r'^api/', include(v1_api.urls)),
)