#!coding=utf-8
#ques
from tastypie.resources import ModelResource
from waimai.models import BusinessArea, Catagory, Goods, Order, OrderItem
from tastypie import fields


class BusinessAreaResource(ModelResource):
    class Meta:
        filtering = {
            "id": ('exact', 'startswith',),
        }
        allowed_methods = ['get']
        queryset=BusinessArea.objects.all()
        resource_name = 'businessarea'
        include_resource_uri = False
        always_return_data = True
        

class OrdersResource(ModelResource):
    class Meta:
        filtering = {
            "id": ('exact', 'startswith',),
            "open_id": ('exact', 'startswith',),
        }
        allowed_methods = ['get']
        queryset=Order.objects.all()
        resource_name = 'order'
        include_resource_uri = False
        always_return_data = True

class CatagoryResource(ModelResource):
    class Meta:
        filtering = {
            "id": ('exact', 'startswith',),
        }
        allowed_methods = ['get']
        queryset=Catagory.objects.all()
        resource_name = 'catagory'
        include_resource_uri = False
        always_return_data = True
        
class GoodsResource(ModelResource):
    Catagory = fields.ForeignKey(CatagoryResource,'catagory',full=True)
    class Meta:
        filtering = {
            "id": ('exact', 'startswith',),
        }
        allowed_methods = ['get']
        queryset=Goods.objects.all()
        resource_name = 'goods'
        include_resource_uri = False
        always_return_data = True

class OrderItemResource(ModelResource):
    goods = fields.ForeignKey(GoodsResource,'goods',full=True,null=True, blank=True)
#     goods = fields.ForeignKey(GoodsResource,'goods',full=True)
    order = fields.ForeignKey(OrdersResource,'order',full=True)
    class Meta:
        filtering = {
            "order":('exact', 'startswith',),
        }
        allowed_methods = ['get']
        queryset=OrderItem.objects.all()
        resource_name = 'orderitem'
        include_resource_uri = False
        always_return_data = True
        
