#!coding=utf-8
'''
Created on 2014-4-30

@author: Administrator
'''
#get
#!/usr/bin/env python

import requests
import json
def get():    
    #limit page num  .
    URL = 'http://127.0.0.1:8000/webserver/api/v1/goods/?format=json'
#    URL = 'http://127.0.0.1:8000/webserver/api/v1/orderitem/?format=json&order=2'
    response = requests.get(URL)
    return response.content
print "get"
print get()
#post
#!/usr/bin/env python
# -*- coding:utf-8 -*-
def post():    
#     URL = 'http://127.0.0.1:8000/webserver/api/v1/catagory/' 
#     values = {'id':'4','name':'tc','postion':'4','shop_id':'1','desc':'saysomething'}
    headers = {'content-type': 'application/json'}
    url = 'http://127.0.0.1:8000/webserver/api/v1/catagory/?format=json'
    data = {'id':'4','name':'tc','postion':'4','shop':{"id":"3"},'desc':'saysomething'}
    response = requests.post(url, data=json.dumps(data), headers=headers)
    print response.content
# print "post"
# print post()


#PUT
def put():
    headers = {'content-type': 'application/json'}
    url = 'http://127.0.0.1:8000/webserver/api/v1/catagory/2/?format=json'
    #token check request
    data = {'name':'肉夹馍','desc':'test','token':'123123'}
    response = requests.put(url, data=json.dumps(data), headers=headers)
    return response.content
# print "put"
# print put()


#delete
import urllib2
def delete():
    request = urllib2.Request("")
    request.get_method = lambda: 'DELETE'
    response = urllib2.urlopen(request)
    
    