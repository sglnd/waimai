#!coding=utf-8
'''
Created on 2014-7-14

@author: Administrator
'''
from django.contrib import admin
from waimai.models import BusinessArea, Catagory, Goods, Order

admin.site.register(BusinessArea)
admin.site.register(Catagory)
admin.site.register(Goods)
admin.site.register(Order)
