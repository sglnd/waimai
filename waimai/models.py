#!coding=utf-8
from django.db import models

class BusinessArea(models.Model):
    """
    商圈
    """
    name=models.CharField(u'商圈名称',max_length=50)
    Latitude=models.CharField(u'经度',max_length=50,null=True)
    Longitude=models.CharField(u'纬度',max_length=50,null=True)
    modified=models.DateTimeField(u'更新时间',auto_now=True)

    def __unicode__(self):
        return '%s' %self.name
    class Meta:
        verbose_name = u'商圈信息'
        verbose_name_plural = '商圈信息管理'
        
class Catagory(models.Model):
    """
    商品类别
    """
    name=models.CharField(u'类别名称',max_length=20)
    postion=models.IntegerField(u'类别顺序')
    desc=models.CharField(u'描述',max_length=50,null=True,blank=True)
    modified=models.DateTimeField(u'更新时间',auto_now=True)
    def __unicode__(self):
        return '%s' %self.name
    class Meta:
        verbose_name = u'商品类别'
        verbose_name_plural = '商品类别管理'
    def belongtoGoods(self):
        return Goods.objects.filter(catagory=self)
    
class Goods(models.Model):
    """
    商品信息
    """
    catagory=models.ForeignKey(Catagory,verbose_name=u'商品类别')
    
    name=models.CharField(u'商品名称',max_length=20)
    price=models.DecimalField(u'商品价格',max_digits=7,decimal_places=2)
    desc=models.CharField(u'描述',max_length=500,null=True,blank=True)
    postion=models.IntegerField(u'排序',null=True,blank=True)
    image_thumb=models.ImageField(u'缩略图地址',upload_to='uploads/tubiao/',null=True,blank=True)
    image=models.ImageField(u'图片地址',upload_to='uploads/image/',null=True,blank=True)
    score=models.IntegerField(u'积分',help_text=u'商品购买后的积分',null=True)
    is_promote=models.BooleanField(u'是否为推荐商品',default=False)
    star=models.IntegerField(u'几星级评价',null=True,default=3)
    sales=models.IntegerField(u'销量',default=0)
    is_book=models.BooleanField(u'是否接受预定')
    fencount = models.IntegerField(u'算几份',default=1)
    modified=models.DateTimeField(u'更新时间',auto_now=True)
    def __unicode__(self):
        return '%s' %self.name
    def save(self, *args, **kwargs):
        super(Goods, self).save(*args, **kwargs)
    class Meta:
        verbose_name = u'商品信息'
        verbose_name_plural = '商品信息管理'

    
class Order(models.Model):
    """
    订单
    """
    XD=u'XD'
    QR=u'QR'
    SD=u'SD'
    SH=u'SH'
    ORDER_STATUS=(
        (XD, u'下单'),
        (QR, u'确认'),
        (SD, u'送单'),
        (SH, u'收货'),
    )
    open_id=models.CharField(max_length=100) #需要加索引
    create_at=models.DateTimeField(u'创建时间',auto_now_add=True)
    customer_remark=models.CharField(u'客户留言',max_length=100,blank=True)
    status=models.CharField(u'订单状态',choices=ORDER_STATUS,default=XD,max_length=100)
    order_remark=models.CharField(u'订单备注',max_length=100,null=True,blank=True)
    price=models.DecimalField(u'价格',max_digits=7,decimal_places=2,default=0)
    count=models.IntegerField(u'商品数量',help_text=u'每份订单数量的总和',default=0)
    comment=models.TextField(verbose_name=u'评论',blank=True,default='')
    
    star=models.IntegerField(u'几星级评价',null=True)
    
    expect_deliver_time=models.DateTimeField(u'期望送货时间',blank=True)
    Latitude=models.CharField(u'经度',max_length=50,blank=True)
    Longitude=models.CharField(u'纬度',max_length=50,blank=True)
    
    send_msg = models.BooleanField(u'是否发送短信',default=False)
    msg_content = models.CharField(u'发送短信内容',null=True,blank=True,max_length=200)
    send_at = models.DateTimeField(u'发送时间',null=True,blank=True)
    modified=models.DateTimeField(u'更新时间',auto_now=True)
    def __unicode__(self):
        return '%s' %self.open_id

    class Meta:
        verbose_name = u'订单'
        verbose_name_plural = '订单管理'

    def getGoodsByOrder(self):
        return OrderItem.objects.filter(order=self)

#     def getItemNames(self):
#         names = OrderItem.objects.filter(order=self).values_list('good__name',flat=True)
#         return ','.join(names)
    
class OrderItem(models.Model):
    """
    订单项目
    """
    good=models.ForeignKey(Goods,verbose_name=u'商品')
    count=models.IntegerField(u'商品数量')
    order=models.ForeignKey(Order,verbose_name=u'订单')
    price=models.DecimalField(u'价格',max_digits=7,decimal_places=2)

    def __unicode__(self):
        return '%s' %self.good.name

    class Meta:
        verbose_name = u'订单详细'
        verbose_name_plural = '订单详细管理'
    
    
